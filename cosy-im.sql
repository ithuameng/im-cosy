SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_hoxin_session
-- ----------------------------
DROP TABLE IF EXISTS `t_hoxin_session`;
CREATE TABLE `t_hoxin_session`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_version` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bind_time` bigint(20) NULL DEFAULT NULL,
  `channel` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `device_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `device_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `host` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `language` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `latitude` double NULL DEFAULT NULL,
  `location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `longitude` double NULL DEFAULT NULL,
  `nid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `os_version` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `state` int(11) NOT NULL,
  `uid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1610988317936668674 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_message_log
-- ----------------------------
DROP TABLE IF EXISTS `t_message_log`;
CREATE TABLE `t_message_log`  (
  `msg_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息唯一ID',
  `routing_key` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路由key',
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息内容',
  `status` int(11) NULL DEFAULT 0 COMMENT '状态：0发送中，1发送成功，2发送失败',
  `count` int(11) NULL DEFAULT 0 COMMENT '重试次数',
  `try_time` datetime NULL DEFAULT NULL COMMENT '第一次重试时间',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`msg_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '记录消息是否发送成功' ROW_FORMAT = COMPACT;

SET FOREIGN_KEY_CHECKS = 1;
