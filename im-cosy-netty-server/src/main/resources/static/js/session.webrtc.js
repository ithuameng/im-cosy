/** 页面加载时，连接netty服务端 **/
window.onload = function () {
    localVideo = document.getElementById('localVideo');
    remoteVideo = document.getElementById('remoteVideo');
    CIMPushManager.connect();
};

async function doShowDialogAndCallVideo(animate) {
    targetName = animate;
    showHTip('发起通话中 ...');
    if (!pc) {
        await createPeerConnection();
    }
    // 发送websocket请求，呼叫被叫方
    let body = new proto.com.ithuameng.cosy.model.proto.Model.SentBody();
    body.setKey(KEY_CLIENT_CALL_VIDEO);
    body.setTimestamp(new Date().getTime());
    body.getDataMap().set("sender", "admin");
    body.getDataMap().set("receiver", targetName);
    body.getDataMap().set("action", "901");
    CIMPushManager.sendRequest(body);
}

// 接受通话
async function handleCalledAccept(message) {
    // 打开本地摄像头
    $('#videoTargetId').text("正在和" + message.sender + "视频中");
    $('#videoDialog').modal({show: true, backdrop: 'static', keyboard: false});
    localStream = await navigator.mediaDevices.getUserMedia({audio: true, video: true});
    localVideo.srcObject = localStream;
    localStream.getTracks().forEach(track => pc.addTrack(track, localStream));

    const offer = await pc.createOffer();
    await pc.setLocalDescription(offer);

    // 发送websocket请求，发送offer给被叫方
    let body = new proto.com.ithuameng.cosy.model.proto.Model.SentBody();
    body.setKey(KEY_CLIENT_CALL_VIDEO);
    body.setTimestamp(new Date().getTime());
    body.getDataMap().set("sender", message.receiver);
    body.getDataMap().set("receiver", message.sender);
    body.getDataMap().set("action", "904");
    body.getDataMap().set("content", JSON.stringify({ type: 'offer', sdp : offer.sdp }));
    CIMPushManager.sendRequest(body);
}

// 拒绝通话
function handleCalledRefuse(message) {
    showETip(message.sender + "拒绝了通话");
    $('#videoDialog').modal("hide");
    doHangup();
}

// 收到answer信息
async function handleCalledAnswer(message) {
    if (!pc) {
        console.error('no peerconnection');
        return;
    }
    await pc.setRemoteDescription(JSON.parse(message.content));
}

function createPeerConnection() {
    pc = new RTCPeerConnection();
    pc.onicecandidate = e => {
        if (e.candidate) {
            // 发送websocket请求，同步icecandidate
            let body = new proto.com.ithuameng.cosy.model.proto.Model.SentBody();
            body.setKey(KEY_CLIENT_CALL_VIDEO);
            body.setTimestamp(new Date().getTime());
            body.getDataMap().set("sender", "admin");
            body.getDataMap().set("receiver", targetName);
            body.getDataMap().set("action", "906");
            body.getDataMap().set("content", JSON.stringify(e.candidate));
            CIMPushManager.sendRequest(body);
        }
    };
    pc.ontrack = e => remoteVideo.srcObject = e.streams[0];
}

// 同步ICE
async function handleCalledCandidate(message) {
    if (!pc) {
        console.error('no peerconnection');
        return;
    }
    let candidate = JSON.parse(message.content);
    if (!candidate.candidate) {
        await pc.addIceCandidate(null);
    } else {
        await pc.addIceCandidate(candidate);
    }
}

