let localVideo;
let remoteVideo;

let localStream;
let pc;

let tmpTargetName;
let targetName;

async function doHangup() {
    if (pc) {
        pc.close();
        pc = null;
    }
    localStream.getTracks().forEach(track => track.stop());
    localStream = null;
}

// 同步ICE
async function handleCalledCandidate(message) {
    if (!pc) {
        console.error('no peerconnection');
        return;
    }
    let candidate = JSON.parse(message.content);
    if (!candidate.candidate) {
        await pc.addIceCandidate(null);
    } else {
        await pc.addIceCandidate(candidate);
    }
}
