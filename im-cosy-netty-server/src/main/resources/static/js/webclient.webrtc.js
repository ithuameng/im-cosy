window.onload = function () {
    localVideo = document.getElementById('localVideo');
    remoteVideo = document.getElementById('remoteVideo');
};

// 被呼叫
function handleCalledParty(message) {
    tmpTargetName = message.sender;
    // 显示弹框
    $('#showCallVideoDialog').modal({show: true, backdrop: 'static', keyboard: false});
}

// 收到offer信息
async function handleCalledOffer(message) {
    // 打开本地摄像头
    $('#videoDialog').modal({show: true, backdrop: 'static', keyboard: false});
    localStream = await navigator.mediaDevices.getUserMedia({audio: true, video: true});
    localVideo.srcObject = localStream;
    localStream.getTracks().forEach(track => pc.addTrack(track, localStream));

    const offer = JSON.parse(message.content);
    console.log("offer", offer);
    await pc.setRemoteDescription(offer);

    const answer = await pc.createAnswer();
    // 发送websocket请求，发送answer给呼叫方
    let body = new proto.com.ithuameng.cosy.model.proto.Model.SentBody();
    body.setKey(KEY_CLIENT_CALL_VIDEO);
    body.setTimestamp(new Date().getTime());
    body.getDataMap().set("sender", message.receiver);
    body.getDataMap().set("receiver", message.sender);
    body.getDataMap().set("action", "905");
    body.getDataMap().set("content", JSON.stringify({type: 'answer', sdp: answer.sdp}));
    CIMPushManager.sendRequest(body);

    await pc.setLocalDescription(answer);
}

// 接受通话
async function handleAccept() {
    if (pc) {
        console.error('existing peerconnection');
        return;
    }
    targetName = tmpTargetName;
    await createPeerConnection();
    let body = new proto.com.ithuameng.cosy.model.proto.Model.SentBody();
    body.setKey(KEY_CLIENT_CALL_VIDEO);
    body.setTimestamp(new Date().getTime());
    body.getDataMap().set("sender", $('#account').val());
    body.getDataMap().set("receiver", "admin");
    body.getDataMap().set("action", "902");
    CIMPushManager.sendRequest(body);
}

// 拒绝通话
function handleRefuse() {
    let body = new proto.com.ithuameng.cosy.model.proto.Model.SentBody();
    body.setKey(KEY_CLIENT_CALL_VIDEO);
    body.setTimestamp(new Date().getTime());
    body.getDataMap().set("sender", $('#account').val());
    body.getDataMap().set("receiver", "admin");
    body.getDataMap().set("action", "903");
    CIMPushManager.sendRequest(body);
}

function createPeerConnection() {
    pc = new RTCPeerConnection();
    pc.onicecandidate = e => {
        if (e.candidate) {
            // 发送websocket请求，同步icecandidate
            let body = new proto.com.ithuameng.cosy.model.proto.Model.SentBody();
            body.setKey(KEY_CLIENT_CALL_VIDEO);
            body.setTimestamp(new Date().getTime());
            body.getDataMap().set("sender", targetName);
            body.getDataMap().set("receiver", "admin");
            body.getDataMap().set("action", "906");
            body.getDataMap().set("content", JSON.stringify(e.candidate));
            CIMPushManager.sendRequest(body);
        }
    };
    pc.ontrack = e => remoteVideo.srcObject = e.streams[0];
}

// 同步ICE
async function handleCalledCandidate(message) {
    if (!pc) {
        console.error('no peerconnection');
        return;
    }
    let candidate = JSON.parse(message.content);
    if (!candidate.candidate) {
        await pc.addIceCandidate(null);
    } else {
        await pc.addIceCandidate(candidate);
    }
}
