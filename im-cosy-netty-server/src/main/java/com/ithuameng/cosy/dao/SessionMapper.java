package com.ithuameng.cosy.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ithuameng.cosy.model.Session;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface SessionMapper extends BaseMapper<Session> {

    @Delete("delete from t_hoxin_session where uid = #{uid} and nid = #{nid}")
    void delete(@Param("uid") String uid, @Param("nid") String nid);

    @Delete("delete from t_hoxin_session")
    void deleteAll();

    @Update("update t_hoxin_session set state = #{state} where uid = #{uid} and nid = #{nid}")
    void updateState(@Param("uid") String uid, @Param("nid") String nid, @Param("state") int state);

    @Update("update t_hoxin_session set state = " + Session.STATE_APNS + " where uid = #{uid} and channel = #{channel}")
    void openApns(@Param("uid") String uid, @Param("channel") String channel);

    @Update("update t_hoxin_session set state = " + Session.STATE_ACTIVE + " where uid = #{uid} and channel = #{channel}")
    void closeApns(@Param("uid") String uid, @Param("channel") String channel);

    @Select("select * from t_hoxin_session where uid != 'admin'")
    List<Session> list();
}
