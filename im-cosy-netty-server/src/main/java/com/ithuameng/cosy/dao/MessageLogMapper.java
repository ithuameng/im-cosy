package com.ithuameng.cosy.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ithuameng.cosy.model.MessageLog;

/**
 * MessageLog dao
 */
public interface MessageLogMapper extends BaseMapper<MessageLog> {

}
