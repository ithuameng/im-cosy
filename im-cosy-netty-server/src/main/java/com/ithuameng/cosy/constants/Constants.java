package com.ithuameng.cosy.constants;

public interface Constants {

    String CIM_MESSAGE_DIRECT_EXCHANGE = "signal/channel/CIM_MESSAGE_DIRECT_EXCHANGE";

    String PUSH_MESSAGE_INNER_QUEUE = "signal/channel/PUSH_MESSAGE_INNER_QUEUE";

    String BIND_MESSAGE_INNER_QUEUE = "signal/channel/BIND_MESSAGE_INNER_QUEUE";

    String BIND_MESSAGE_ROUTING_KEY = "bind";

    String PUSH_MESSAGE_ROUTING_KEY = "push";

    String MESSAGE_CONSUME_REDIS_RECORD = "message_consume_redis_record";

    String FORCE_OFFLINE_ACTION = "999";

    String SYSTEM_ID = "0";
}
