package com.ithuameng.cosy.component.handler;

import com.ithuameng.cosy.component.handler.annotation.CIMHandler;
import com.ithuameng.cosy.mq.signal.SignalRabbitTemplate;
import com.ithuameng.cosy.model.Session;
import com.ithuameng.cosy.constant.ChannelAttr;
import com.ithuameng.cosy.group.SessionGroup;
import com.ithuameng.cosy.handler.CIMRequestHandler;
import com.ithuameng.cosy.model.ReplyBody;
import com.ithuameng.cosy.model.SentBody;
import com.ithuameng.cosy.service.ISessionService;
import io.netty.channel.Channel;
import org.springframework.http.HttpStatus;

import javax.annotation.Resource;

/**
 * 客户长连接 账户绑定实现
 */
@CIMHandler(key = "client_bind")
public class BindHandler implements CIMRequestHandler {

    @Resource
    private ISessionService sessionService;

    @Resource
    private SessionGroup sessionGroup;

    @Resource
    private SignalRabbitTemplate signalRabbitTemplate;

    @Override
    public void process(Channel channel, SentBody body) {

        ReplyBody reply = new ReplyBody();
        reply.setKey(body.getKey());
        reply.setCode(HttpStatus.OK.value());
        reply.setTimestamp(System.currentTimeMillis());

        String uid = body.get("uid");
        Session session = new Session();
        session.setUid(uid);
        session.setNid(channel.attr(ChannelAttr.ID).get());
        session.setDeviceId(body.get("deviceId"));
        session.setChannel(body.get("channel"));
        session.setDeviceName(body.get("deviceName"));
        session.setAppVersion(body.get("appVersion"));
        session.setOsVersion(body.get("osVersion"));
        session.setLanguage(body.get("language"));

        channel.attr(ChannelAttr.UID).set(uid);
        channel.attr(ChannelAttr.CHANNEL).set(session.getChannel());
        channel.attr(ChannelAttr.DEVICE_ID).set(session.getDeviceId());
        channel.attr(ChannelAttr.LANGUAGE).set(session.getLanguage());

		/*
         *存储到数据库
		 */
        sessionService.add(session);

		/*
         * 添加到内存管理
		 */
        sessionGroup.add(channel);

		/*
         *向客户端发送bind响应
		 */
        channel.writeAndFlush(reply);

		/*
         * 发送上线事件到集群中的其他实例，控制其他设备下线
		 */
        signalRabbitTemplate.bind(session);
    }
}
