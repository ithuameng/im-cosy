package com.ithuameng.cosy.component.handler;

import com.ithuameng.cosy.component.handler.annotation.CIMHandler;
import com.ithuameng.cosy.group.SessionGroup;
import com.ithuameng.cosy.handler.CIMRequestHandler;
import com.ithuameng.cosy.model.Message;
import com.ithuameng.cosy.model.SentBody;
import io.netty.channel.Channel;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 视频通话，用于action消息类型转发
 */
@CIMHandler(key = "client_call_video")
public class VideoHandler implements CIMRequestHandler {

    @Resource
    private SessionGroup sessionGroup;

    @Override
    public void process(Channel channel, SentBody body) {
        Map<String, String> data = body.getData();
        Message msg = new Message();
        msg.setId(System.currentTimeMillis());
        msg.setSender(data.get("sender"));
        msg.setReceiver(data.get("receiver"));
        msg.setAction(data.get("action"));
        msg.setContent(data.get("content"));
        sessionGroup.write(msg.getReceiver(), msg);
    }
}
