package com.ithuameng.cosy.component.push;

import com.ithuameng.cosy.mq.signal.SignalRabbitTemplate;
import com.ithuameng.cosy.model.Message;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 消息发送实现类
 */
@Component
public class DefaultMessagePusher implements CIMMessagePusher {

    @Resource
    private SignalRabbitTemplate signalRabbitTemplate;

    /**
     * 向用户发送消息
     *
     * @param message
     */
    @Override
    public final void push(Message message) {

		/*
         * 通过发送redis广播，到集群中的每台实例，获得当前UID绑定了连接并推送
		 * @see com.farsunset.hoxin.component.message.PushMessageListener
		 */
        signalRabbitTemplate.push(message);
    }
}
