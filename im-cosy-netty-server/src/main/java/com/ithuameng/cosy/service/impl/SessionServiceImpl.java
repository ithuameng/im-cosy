package com.ithuameng.cosy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ithuameng.cosy.model.Session;
import com.ithuameng.cosy.dao.SessionMapper;
import com.ithuameng.cosy.service.ISessionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class SessionServiceImpl extends ServiceImpl<SessionMapper, Session> implements ISessionService {

    private final String host;

    public SessionServiceImpl() throws UnknownHostException {
        host = InetAddress.getLocalHost().getHostAddress();
    }

    @Override
    public void add(Session session) {
        session.setBindTime(System.currentTimeMillis());
        session.setHost(host);
        super.save(session);
    }

    @Override
    public void delete(String uid, String nid) {
        super.baseMapper.delete(uid, nid);
    }

    @Override
    public void deleteAll() {
        super.baseMapper.deleteAll();
    }

    @Override
    public void updateState(String uid, String nid, int state) {
        super.baseMapper.updateState(uid, nid, state);
    }

    @Override
    public List<Session> findAll() {
        return super.baseMapper.list();
    }
}
