package com.ithuameng.cosy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ithuameng.cosy.model.MessageLog;

import java.util.List;

/**
 * MessageLog service
 */
public interface IMessageLogService extends IService<MessageLog> {

    // 获取发送失败的消息
    List<MessageLog> getByStatus();

    // 更新状态
    void updateStatus(String msgId, Integer status);

    // 更新重试数量
    void updateCount(String msgId);

    // 保存新的消息日志
    void saveMessageLog(String msgId, String routingKey, String content);
}
