package com.ithuameng.cosy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ithuameng.cosy.dao.MessageLogMapper;
import com.ithuameng.cosy.model.MessageLog;
import com.ithuameng.cosy.service.IMessageLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * MessageLog service implementation
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class MessageLogServiceImpl extends ServiceImpl<MessageLogMapper, MessageLog> implements IMessageLogService {

    @Override
    public List<MessageLog> getByStatus() {
        QueryWrapper<MessageLog> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(MessageLog.STATUS, 0);
        queryWrapper.lt(MessageLog.TRY_TIME, LocalDateTime.now());
        return super.list(queryWrapper);
    }

    @Override
    public void updateStatus(String msgId, Integer status) {
        MessageLog messageLog = new MessageLog();
        messageLog.setMsgId(msgId);
        messageLog.setStatus(status);
        super.updateById(messageLog);
    }

    @Override
    public void updateCount(String msgId) {
        MessageLog messageLog = super.getById(msgId);
        messageLog.setCount(messageLog.getCount() + 1);
        messageLog.setUpdateTime(LocalDateTime.now());
        super.updateById(messageLog);
    }

    @Override
    public void saveMessageLog(String msgId, String routingKey, String content) {
        MessageLog messageLog = new MessageLog();
        messageLog.setMsgId(msgId);
        messageLog.setRoutingKey(routingKey);
        messageLog.setContent(content);
        messageLog.setTryTime(LocalDateTime.now().plusMinutes(1));
        messageLog.setCreateTime(LocalDateTime.now());
        super.save(messageLog);
    }
}
