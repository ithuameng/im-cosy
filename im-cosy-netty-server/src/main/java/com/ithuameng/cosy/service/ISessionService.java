package com.ithuameng.cosy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ithuameng.cosy.model.Session;

import java.util.List;

/**
 * 存储连接信息，便于查看用户的链接信息
 */
public interface ISessionService extends IService<Session> {

	void add(Session session);

	void delete(String uid, String nid);

	void deleteAll();

	void updateState(String uid, String nid, int state);

	List<Session> findAll();
}
