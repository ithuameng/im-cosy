package com.ithuameng.cosy.task;

import com.ithuameng.cosy.constants.Constants;
import com.ithuameng.cosy.model.MessageLog;
import com.ithuameng.cosy.service.IMessageLogService;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class MessageSendTask {

    @Resource
    private RabbitTemplate rabbitTemplate;

    @Resource
    private IMessageLogService messageLogService;

    @Scheduled(cron = "0/10 * * * * ?")
    public void messageSendTask() {
        List<MessageLog> logs = messageLogService.getByStatus();
        if (logs.size() == 0) {
            return;
        }
        logs.forEach(item -> {
            if (item.getCount() >= 3) {
                // 消息发送失败
                messageLogService.updateStatus(item.getMsgId(), 2);
            } else {
                // 更新重试数量
                messageLogService.updateCount(item.getMsgId());
                // 发送消息
                rabbitTemplate.convertAndSend(Constants.CIM_MESSAGE_DIRECT_EXCHANGE, item.getRoutingKey(),
                        item.getContent(), new CorrelationData(item.getMsgId()));
            }
        });
    }
}
