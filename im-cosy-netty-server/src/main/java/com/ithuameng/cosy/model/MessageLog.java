package com.ithuameng.cosy.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 记录消息是否发送成功
 */
@Data
@TableName("t_message_log")
@EqualsAndHashCode(callSuper = false)
public class MessageLog implements Serializable {

    private static final long serialVersionUID = 1820092352323100283L;

    public static final String MSG_ID = "msg_id";
    public static final String STATUS = "status";
    public static final String COUNT = "count";
    public static final String TRY_TIME = "try_time";
    public static final String CREATE_TIME = "create_time";
    public static final String UPDATE_TIME = "try_time";

    /**
     * 消息唯一ID
     */
    @TableId(type = IdType.INPUT)
    private String msgId;

    /**
     * 路由key
     */
    private String routingKey;

    /**
     * 消息内容
     */
    private String content;

    /**
     * 状态：0发送中，1发送成功，2发送失败
     */
    private Integer status;

    /**
     * 重试数量
     */
    private Integer count;

    /**
     * 第一次尝试时间
     */
    private LocalDateTime tryTime;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;
}
