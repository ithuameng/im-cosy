package com.ithuameng.cosy.config.properties;

import com.ithuameng.cosy.constant.WebsocketProtocol;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "cim.websocket")
public class CIMWebsocketProperties {

    private boolean enable;

    private Integer port;

    private String path;

    private WebsocketProtocol protocol;

    private boolean sslEnable;

    private String sslType;

    private String sslFileName;

    private String sslPassword;

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public WebsocketProtocol getProtocol() {
        return protocol;
    }

    public void setProtocol(WebsocketProtocol protocol) {
        this.protocol = protocol;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public boolean isSslEnable() {
        return sslEnable;
    }

    public void setSslEnable(boolean sslEnable) {
        this.sslEnable = sslEnable;
    }

    public String getSslType() {
        return sslType;
    }

    public void setSslType(String sslType) {
        this.sslType = sslType;
    }

    public String getSslFileName() {
        return sslFileName;
    }

    public void setSslFileName(String sslFileName) {
        this.sslFileName = sslFileName;
    }

    public String getSslPassword() {
        return sslPassword;
    }

    public void setSslPassword(String sslPassword) {
        this.sslPassword = sslPassword;
    }
}
