package com.ithuameng.cosy.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;

import java.util.Objects;

@Configuration
public class RedisConfig {

    @Autowired
    public RedisConfig(LettuceConnectionFactory connectionFactory, @Value("${spring.profiles.active}") String profile) {
        if (Objects.equals("dev", profile)) {
            connectionFactory.setValidateConnection(true);
        }
    }
}
