package com.ithuameng.cosy.group;

import com.ithuameng.cosy.constant.ChannelAttr;
import io.netty.channel.Channel;

public class TagSessionGroup extends SessionGroup {

    @Override
    protected String getKey(Channel channel) {
        return channel.attr(ChannelAttr.TAG).get();
    }
}