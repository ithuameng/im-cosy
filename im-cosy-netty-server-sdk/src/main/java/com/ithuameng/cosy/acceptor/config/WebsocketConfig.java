package com.ithuameng.cosy.acceptor.config;

import com.ithuameng.cosy.constant.WebsocketProtocol;
import com.ithuameng.cosy.handler.CIMRequestHandler;
import com.ithuameng.cosy.handshake.HandshakeEvent;

import java.util.function.Predicate;

/**
 * Websocket Config
 *
 * @author ithuameng
 */
public class WebsocketConfig {

    private static final int DEFAULT_PORT = 34567;

    private static final String DEFAULT_PATH = "/";

    private static final WebsocketProtocol DEFAULT_PROTOCOL = WebsocketProtocol.PROTOBUF;

    private Integer port;
    private String path;
    private WebsocketProtocol protocol;
    private CIMRequestHandler outerRequestHandler;
    private Predicate<HandshakeEvent> handshakePredicate;
    private boolean enable;
    private String sslType;
    private String sslPath;
    private String sslPassword;
    private boolean sslEnable;

    public Integer getPort() {
        return port == null ? DEFAULT_PORT : port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getPath() {
        return path == null ? DEFAULT_PATH : path;
    }

    public WebsocketProtocol getProtocol() {
        return protocol == null ? DEFAULT_PROTOCOL : protocol;
    }

    public CIMRequestHandler getOuterRequestHandler() {
        return outerRequestHandler;
    }

    public Predicate<HandshakeEvent> getHandshakePredicate() {
        return handshakePredicate;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setProtocol(WebsocketProtocol protocol) {
        this.protocol = protocol;
    }

    public void setOuterRequestHandler(CIMRequestHandler outerRequestHandler) {
        this.outerRequestHandler = outerRequestHandler;
    }

    public void setHandshakePredicate(Predicate<HandshakeEvent> handshakePredicate) {
        this.handshakePredicate = handshakePredicate;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getSslType() {
        return sslType;
    }

    public void setSslType(String sslType) {
        this.sslType = sslType;
    }

    public String getSslPath() {
        return sslPath;
    }

    public void setSslPath(String sslPath) {
        this.sslPath = sslPath;
    }

    public String getSslPassword() {
        return sslPassword;
    }

    public void setSslPassword(String sslPassword) {
        this.sslPassword = sslPassword;
    }

    public boolean isSslEnable() {
        return sslEnable;
    }

    public void setSslEnable(boolean sslEnable) {
        this.sslEnable = sslEnable;
    }
}
