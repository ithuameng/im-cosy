package com.ithuameng.cosy.exception;

/**
 * 读取无效类型异常
 *
 * @author ithuameng
 */
public class ReadInvalidTypeException extends RuntimeException {

    public ReadInvalidTypeException(byte type) {
        super("Read invalid tag : " + type);
    }
}
