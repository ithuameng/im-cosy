package com.ithuameng.cosy.coder.json;

import com.ithuameng.cosy.constant.ChannelAttr;
import com.ithuameng.cosy.constant.DataType;
import com.ithuameng.cosy.exception.ReadInvalidTypeException;
import com.ithuameng.cosy.model.Pong;
import com.ithuameng.cosy.model.SentBody;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.json.JsonReadFeature;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

import java.util.List;

/**
 * websocket协议 json消息体解码
 *
 * @author ithuameng
 */
public class TextMessageDecoder extends MessageToMessageDecoder<TextWebSocketFrame> {

    private static final JsonMapper OBJECT_MAPPER = JsonMapper.builder()
            .enable(JsonReadFeature.ALLOW_UNESCAPED_CONTROL_CHARS)
            .enable(JsonReadFeature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER)
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
            .serializationInclusion(JsonInclude.Include.NON_NULL)
            .build();

    @Override
    protected void decode(ChannelHandlerContext context, TextWebSocketFrame frame, List<Object> list) throws Exception {

        context.channel().attr(ChannelAttr.PING_COUNT).set(null);

        String text = frame.text();

        TransmitBody protocol = OBJECT_MAPPER.readValue(text, TransmitBody.class);

        if (protocol.getType() == DataType.PONG.getValue()) {
            list.add(Pong.getInstance());
            return;
        }

        if (protocol.getType() == DataType.SENT.getValue()) {
            SentBody body = OBJECT_MAPPER.readValue(protocol.getContent(), SentBody.class);
            list.add(body);
            return;
        }

        throw new ReadInvalidTypeException(protocol.getType());
    }
}