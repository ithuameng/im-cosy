package com.ithuameng.cosy.coder.json;

/**
 * json协议下的消息结构
 *
 * @author ithuameng
 */
class TransmitBody {

    private byte type;

    private String content;

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
