package com.ithuameng.cosy.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 客户端请求结构
 */
public class SentBody implements Serializable {

    private static final long serialVersionUID = 1L;

    private String key;

    private final Map<String, String> data = new HashMap<>();

    private long timestamp;

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data.clear();
        this.data.putAll(data);
    }

    public String getKey() {
        return key;
    }

    public String get(String key) {
        return data.get(key);
    }

    public String get(String key, String defaultValue) {
        return data.getOrDefault(key, defaultValue);
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[SentBody]").append("\n");
        builder.append("key       :").append(this.getKey()).append("\n");
        builder.append("timestamp :").append(timestamp).append("\n");
        builder.append("data      :").append("\n{");
        data.forEach((k, v) -> builder.append("\n").append(k).append(":").append(v));
        builder.append(data.isEmpty() ? "" : "\n").append("}");
        return builder.toString();
    }
}
