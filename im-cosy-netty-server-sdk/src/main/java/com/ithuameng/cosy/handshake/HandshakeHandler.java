package com.ithuameng.cosy.handshake;

import com.ithuameng.cosy.constant.CIMConstant;
import com.ithuameng.cosy.model.ReplyBody;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;

import java.util.function.Predicate;

/**
 * WS握手时鉴权
 *
 * @author ithuameng
 */
@ChannelHandler.Sharable
public class HandshakeHandler extends ChannelInboundHandlerAdapter {
    /*
     *认证失败，返回replyBody给客户端
     */
    private final ReplyBody failedBody = ReplyBody.make(CIMConstant.CLIENT_HANDSHAKE,
            HttpResponseStatus.UNAUTHORIZED.code(),
            HttpResponseStatus.UNAUTHORIZED.reasonPhrase());

    private final Predicate<HandshakeEvent> handshakePredicate;

    public HandshakeHandler(Predicate<HandshakeEvent> handshakePredicate) {
        this.handshakePredicate = handshakePredicate;
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {

        super.userEventTriggered(ctx, evt);

        if (evt instanceof WebSocketServerProtocolHandler.HandshakeComplete) {
            doAuthentication(ctx, (WebSocketServerProtocolHandler.HandshakeComplete) evt);
        }
    }

    private void doAuthentication(ChannelHandlerContext context, WebSocketServerProtocolHandler.HandshakeComplete event) {

        if (handshakePredicate == null) {
            return;
        }

        /*
         * 鉴权不通过，发送响应体并关闭链接
         */
        if (!handshakePredicate.test(HandshakeEvent.of(event))) {
            context.channel().writeAndFlush(failedBody).addListener(ChannelFutureListener.CLOSE);
        }
    }
}