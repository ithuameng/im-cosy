package com.ithuameng.cosy.constant;

import io.netty.util.AttributeKey;

/**
 * channel属性定义 - 线程安全
 *
 * @author ithuameng
 */
public interface ChannelAttr {
    AttributeKey<Integer> PING_COUNT = AttributeKey.valueOf("ping_count");
    AttributeKey<String> UID = AttributeKey.valueOf("uid");
    AttributeKey<String> CHANNEL = AttributeKey.valueOf("channel");
    AttributeKey<String> ID = AttributeKey.valueOf("id");
    AttributeKey<String> DEVICE_ID = AttributeKey.valueOf("device_id");
    AttributeKey<String> TAG = AttributeKey.valueOf("tag");
    AttributeKey<String> LANGUAGE = AttributeKey.valueOf("language");
}
