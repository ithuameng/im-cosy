package com.ithuameng.cosy.constant;

/**
 * 常量
 *
 * @author ithuameng
 */
public interface CIMConstant {

    /**
     * 消息头长度为3个字节，第一个字节为消息类型，第二，第三字节 转换int后为消息长度
     */
    byte DATA_HEADER_LENGTH = 3;

    String CLIENT_CONNECT_CLOSED = "client_closed";

    String CLIENT_HANDSHAKE = "client_handshake";
}
