package com.ithuameng.cosy.constant;

/**
 * 数据类型
 *
 * @author ithuameng
 */
public enum DataType {

    /**
     * 客户端发送的的心跳响应
     */
    PONG(0),

    /**
     * 服务端端发送的心跳响应
     */
    PING(1),

    /**
     * 服务端端发送的消息体
     */
    MESSAGE(2),

    /**
     * 客户端发送的请求体
     */
    SENT(3),

    /**
     * 服务端端发送的响应体
     */
    REPLY(4);

    private final byte value;

    DataType(int value) {
        this.value = (byte) value;
    }

    public byte getValue() {
        return value;
    }
}
