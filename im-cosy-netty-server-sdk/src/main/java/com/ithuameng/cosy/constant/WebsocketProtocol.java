package com.ithuameng.cosy.constant;

/**
 * websocket 支持的消息体协议
 *
 * @author ithuameng
 */
public enum WebsocketProtocol {
    PROTOBUF, JSON
}
