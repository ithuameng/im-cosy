package com.ithuameng.cosy.handler;

import com.ithuameng.cosy.model.SentBody;
import io.netty.channel.Channel;

/**
 * 请求处理接口,所有的请求实现必须实现此接口
 *
 * @author ithuameng
 */
public interface CIMRequestHandler {

    /**
     * 处理收到客户端从长链接发送的数据
     */
    void process(Channel channel, SentBody body);
}
